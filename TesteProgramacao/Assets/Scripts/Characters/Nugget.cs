﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Nugget : MonoBehaviour
{
    [SerializeField] private Text _nuggetSpeech;
    [SerializeField] private List<string> _speechs;
    public void ChangeSpeech(int _speechId)
    {
        _nuggetSpeech.text = _speechs[_speechId];
    }    
}
