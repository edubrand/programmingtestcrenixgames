﻿///<summary>
///This class is to handle with drag and drop mechanics
///</summary>
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
[RequireComponent(typeof(DragDropUI))]
public class DragDropSystem : MonoBehaviour,IBeginDragHandler,IDragHandler,IEndDragHandler
{        
    #region ReferencesVariables
    private DragDropUI _dragDropUI;
    private CogPlacementController _cogPlacementController;
    private Collider2D _collider;
    #endregion
    #region DragVariables
        #region Slot object, position
        private GameObject _currentSlot;
        private Transform _currentPlacement;
        private Vector3 _placementPosition;
        #endregion
        #region UI drag balance variables
        [Tooltip("This variable is used to change the alpha of the object that being dragged")] 
        [SerializeField] private float _draggedImageAlpha = 1;
        [Tooltip("This variable is used to return the original alpha of the object")]
        [SerializeField] private float _undraggedImageAlpha = 1;
    #endregion
    #endregion
    #region RaycastVariables
    private List<RaycastResult> raycastResults;
    #endregion
    private void Awake() {
        GetReferences();
    }
    #region MouseEvents
    public void OnBeginDrag(PointerEventData eventData)
    {
        BeginDragItemHandler(eventData);
        SetupUI(true);
    }
    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        EndDragItemHandler(eventData);     
        SetupUI(false);
        //Move to placement position
        transform.position = _placementPosition;
    }
    #region DragSupportMethods
    private void BeginDragItemHandler(PointerEventData eventData)
    { 
        _currentSlot = GetCurrentItemSlot();
        if (_currentSlot != null)
        {
            //Get the original position and object
            _currentPlacement = _currentSlot.transform;
            _placementPosition = _currentPlacement.transform.position;
            //If the object is in the world send it to canvas, remove it from cog placement list and reactivate slot collider
            if (_currentSlot.CompareTag("Placement"))
            {
                _dragDropUI.SendObjectToCanvas();
                CogPlacement _script =  _currentSlot.GetComponent<CogPlacement>();
                if (!_script.IsColliderActivate())
                {
                    _script.ToggleCollider();    
                }
                _dragDropUI.ResetObjectScale();
                _cogPlacementController.RemoveCog(gameObject);
                _cogPlacementController.CheckCogOnList();
            }
            _dragDropUI.SendObjectToLowestCanvasHierarchy();
        }
    }
    private void EndDragItemHandler(PointerEventData eventData)
    {
        //Disable own collider
        ToggleCollider();
        GameObject dropTarget = GetRaycastResultObject(eventData);
        if (dropTarget != null && (dropTarget.CompareTag("Placement") || dropTarget.CompareTag("InventorySlot")))
        {
            //Get the new position and object
            _currentPlacement = dropTarget.transform;
            _placementPosition = _currentPlacement.position;
            //If target is in the world deactivate slot collider and add object to cog placement list
            if (dropTarget.CompareTag("Placement"))
            {
                CogPlacement _script =  dropTarget.GetComponent<CogPlacement>();
                if (_script.IsColliderActivate())
                {
                    _script.ToggleCollider();    
                }
                _cogPlacementController.AddCog(gameObject);
            }
        }
        else
        {
            //if theres no drop target and object is on placement return the object to placement list
            if (_currentSlot.CompareTag("Placement"))
            {
                _cogPlacementController.AddCog(gameObject);
            }
        }
        //Return the object to parent
        _dragDropUI.ReturnObjectToCurrentParent(_currentPlacement);
        //Reset object scale
        _dragDropUI.ResetObjectScale();
        //Check object list
        _cogPlacementController.CheckCogOnList();   
        //Enable own collider
        ToggleCollider();
    }
    private void SetupUI(bool isBegin)
    { 
        if (isBegin)
        {
            //Confine cursor to window screen
            _dragDropUI.ConfineCursor(true);
            //Disable Raycast on object being dragged
            _dragDropUI.ToggleRaycastTarget();
            //Make object transparency
            _dragDropUI.ChangeAlpha(_draggedImageAlpha);
        }
        else
        {
            //Enable cursor to move freely
            _dragDropUI.ConfineCursor(false);
            //Enable Raycast on object that was dropped
            _dragDropUI.ToggleRaycastTarget();
            //Return to original transparency
            _dragDropUI.ChangeAlpha(_undraggedImageAlpha);
        }
    }
    #endregion
    #endregion
    #region ObjectMethods
    private GameObject GetRaycastResultObject(PointerEventData eventData)
    {
        //Check UI elements
        raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData,raycastResults);
        if (raycastResults.Count != 0 && raycastResults[0].gameObject != null)
        {
            return raycastResults[0].gameObject;
        }
        //Check World objects
        float rayDistance = 1;
        RaycastHit2D[] hit = Physics2D.RaycastAll(Camera.main.ScreenToWorldPoint(Input.mousePosition),Vector2.up,rayDistance);
        if (hit.Length != 0)
        {
            return hit[0].collider.gameObject;
        }
        return null;
    }
    private GameObject GetCurrentItemSlot()
    {
        GameObject itemSlot = transform.parent.gameObject;
        if (itemSlot != null)
        {
            return itemSlot;
        }
        return null;
    }
    private void ToggleCollider()
    {
        _collider.enabled = !_collider.enabled;
    }
    #endregion
    #region Utilities
    private void GetReferences()
    {
        _dragDropUI = GetComponent<DragDropUI>();
        _collider = GetComponent<Collider2D>();
        _cogPlacementController = CogPlacementController.Instance;
    }
    #endregion 
}
