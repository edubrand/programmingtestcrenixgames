﻿///<summary>
/// This class has the unique and exclusive function of editing the UI element component
///</summary>
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DragDropUI : MonoBehaviour
{
    private Image _objImage;
    private Canvas _canvas;
    private void Awake() 
    {
        _objImage = GetComponent<Image>();
        _canvas = FindObjectOfType<Canvas>();
    }
    public void ToggleRaycastTarget()
    {
        _objImage.raycastTarget = !_objImage.raycastTarget;
    }
    public void ChangeAlpha(float value)
    {
        _objImage.color = new Color(_objImage.color.r,_objImage.color.g,_objImage.color.b,value);
    }
    public void ConfineCursor(bool cursorConfined)
    {
        if(cursorConfined)
        {
            Cursor.lockState = CursorLockMode.Confined;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
        }
    }
    public void ResetObjectScale()
    {
        transform.localScale = Vector3.one;
    }
    public void SendObjectToLowestCanvasHierarchy()
    {
        transform.SetParent(transform.root);
    }
    public void SendObjectToCanvas()
    {
        transform.SetParent(_canvas.transform);
    }
    public void ReturnObjectToCurrentParent(Transform currentParent)
    {
        transform.SetParent(currentParent);
    }
}
