﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CogPlacement : MonoBehaviour
{
    [SerializeField] private RotationDirection _rotationDirection;
    private GameObject _objectTarget;
    private Collider2D _collider;
    private Animator _anim;
    private void Awake() {
        _collider = GetComponent<Collider2D>();
    }
    public void GetObjectOnPlacement()
    {
        if (transform.childCount != 0)
        {
            _objectTarget = transform.GetChild(0).gameObject;
            _anim = _objectTarget.GetComponent<Animator>();
        }
    }
    public bool IsColliderActivate()
    {
        return _collider.enabled;
    }
    public void ToggleCollider()
    {
        _collider.enabled = !_collider.enabled;
    }
    public void ActivateObjectAnimation()
    {
        if (_anim != null)
        {
            if (_rotationDirection == RotationDirection.clockwise)
            {
                _anim.Play("Clockwise");
            }
            else
            {
                _anim.Play("AntiClockwise");
            }
        }
    }
    public void DeactivateObjectAnimation()
    {
        if (_anim != null)
        {
            _anim.Play("Idle");
        }
    }
    public enum RotationDirection
    {
        clockwise,
        anticlowise
    }

}
