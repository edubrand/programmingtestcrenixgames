﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CogPlacementController : Singleton<CogPlacementController>
{
    [SerializeField] private List<GameObject> cogPlacements = new List<GameObject>();
    [SerializeField] private List<GameObject> cogs = new List<GameObject>();
    [SerializeField] private Nugget _nugget;
    private void Awake() {
        FillCogSlotList();
    }
    private void FillCogSlotList()
    {
        GameObject[] objects = GameObject.FindGameObjectsWithTag("Placement");
        for (int i =0;i<objects.Length;i++)
        {
            cogPlacements.Add(objects[i]);
        }
    }
    public void AddCog(GameObject cog)
    {
        cogs.Add(cog);
    }
    public void RemoveCog(GameObject cog)
    {
        cogs.Remove(cog);
    }
    public void ResetCogsPlacement()
    {
        //Return cogs to inventory slot
        GameObject[] inventorySlot = GameObject.FindGameObjectsWithTag("InventorySlot");
        for (int i = 0;i<inventorySlot.Length;i++)
        {
            if (inventorySlot[i].transform.childCount == 0)
            {
                GameObject targetCog = cogs[0];
                targetCog.transform.SetParent(inventorySlot[i].transform);
                targetCog.transform.position = inventorySlot[i].transform.position;
                targetCog.transform.localScale = Vector3.one;
                cogs.RemoveAt(0);
            }
        }
        //Reset cogs placement colliders
        for (int i = 0; i<cogPlacements.Count;i++)
        {
            CogPlacement _script = cogPlacements[i].GetComponent<CogPlacement>();
            if(!_script.IsColliderActivate())
            {
                _script.ToggleCollider();
            }
        }
        CheckCogOnList();
    }
    public void CheckCogOnList()
    {
        if (cogs.Count == cogPlacements.Count)
        {
            for (int i = 0; i<cogPlacements.Count;i++)
            {                
                CogPlacement _script = cogPlacements[i].GetComponent<CogPlacement>();
                _script.GetObjectOnPlacement();
                _script.ActivateObjectAnimation();
            }
            _nugget.ChangeSpeech(1);
        }
        else
        {
            for (int i = 0; i<cogPlacements.Count;i++)
            {                
                CogPlacement component = cogPlacements[i].GetComponent<CogPlacement>();
                component.GetObjectOnPlacement();
                component.DeactivateObjectAnimation();
            }
            _nugget.ChangeSpeech(0);
        }
    }

}
