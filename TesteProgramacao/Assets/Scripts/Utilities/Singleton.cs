﻿using UnityEngine;
/// <summary>
/// Inherit from this base class to create a singleton.
/// e.g. public class MyClassName : Singleton<MyClassName>
/// </summary>
public class Singleton<T> : MonoBehaviour where T: MonoBehaviour
{
    private static T _instance;
    private static readonly object Lock = new object();
    [SerializeField]
    private bool _persistent = true;
    public static T Instance
    {
        get
        {
            lock (Lock)
            {
                if (_instance != null)
                    return _instance;
                var instances = FindObjectsOfType<T>();
                var count = instances.Length;
                if (count > 0)
                {
                    if (count == 1)
                        return _instance = instances[0];
                    for (var i = 1; i < instances.Length; i++)
                        Destroy(instances[i]);
                    return _instance = instances[0];
                }
                return _instance = new GameObject().AddComponent<T>();
            }
        }
    }
   
    private void Awake()
    {
        if (_persistent)
            DontDestroyOnLoad(gameObject);
    }
}